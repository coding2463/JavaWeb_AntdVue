// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.generator.utils;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据表列
 */
@Data
public class ColumnClass {

    /**
     * 数据库字段名称
     */
    private String columnName;

    /**
     * 实体对象字段名称
     */
    private String columnName2;

    /**
     * 数据库字段类型
     */
    private String columnType;

    /**
     * 数据库字段首字母小写且去掉下划线字符串
     */
    private String changeColumnName;

    /**
     * 数据库字段注释
     */
    private String columnComment;

    /**
     * 是否有注解值
     */
    private boolean hasColumnCommentValue;

    /**
     * 是否选择开关
     */
    private boolean columnSwitch;

    /**
     * 数据库字段注释(仅包含名称)
     */
    private String columnCommentName;

    /**
     * 数据库字段注解值
     */
    private Map<String, String> columnCommentValue = new HashMap<>();

    /**
     * 字段值(如：1淘宝 2京东 3拼多多,需转换成：1=淘宝,2=京东,3=拼多多)
     */
    private String columnValue;

    /**
     * 字段值列表
     */
    private Map<Integer, String> columnValueList;

    /**
     * 列类型样式(颜色)列表
     */
    private Map<String, String> columnValueStyleList;

    /**
     * 字段配置至是否是数字（特殊情况下，值不一定是数字，如：hidden=隐藏）
     */
    private boolean columnNumberValue;

    /**
     * 字段值开关(如：淘宝|京东)
     */
    private String columnSwitchValue;

    /**
     * 字段默认值
     */
    private String columnDefaultValue;

    /**
     * 是否是图片字段
     */
    private boolean columnImage;

    /**
     * 是否是多行文本
     */
    private boolean columnTextArea;

}
