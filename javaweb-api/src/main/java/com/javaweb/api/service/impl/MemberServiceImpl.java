// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.api.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javaweb.api.dto.LoginDto;
import com.javaweb.api.entity.Member;
import com.javaweb.api.mapper.MemberMapper;
import com.javaweb.api.service.IMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.JwtUtil;
import com.javaweb.common.utils.StringUtils;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.javaweb.common.utils.JwtUtil.parseJWT;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 鲲鹏
 * @date 2021-10-12
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements IMemberService {

    @Autowired
    private MemberMapper memberMapper;

    /**
     * 会员登录
     *
     * @param loginDto 参数
     * @return
     */
    @Override
    public JsonResult login(LoginDto loginDto) {
        // 会员账号
        if (StringUtils.isEmpty(loginDto.getUsername())) {
            return JsonResult.error("会员账号不能为空");
        }
        // 会员密码
        if (StringUtils.isEmpty(loginDto.getPassword())) {
            return JsonResult.error("会员密码不能为空");
        }
        // 查询用户信息
        Member member = memberMapper.selectOne(new LambdaQueryWrapper<Member>()
                .eq(Member::getUsername, loginDto.getUsername())
                .eq(Member::getMark, 1)
                .last("limit 1"));
        if (StringUtils.isNull(member)) {
            return JsonResult.error("用户不存在");
        }
        // 密码校验
        if (!member.getPassword().equals(CommonUtils.password(loginDto.getPassword()))) {
            return JsonResult.error("用户密码不正确");
        }
        // 用户状态验证
        if (!member.getStatus().equals(1)) {
            return JsonResult.error("您的账号已被禁用");
        }
        // 返回Token
        String token = JwtUtil.createJWT(member.getId());
        System.out.println("token值：" + token);
        // Jwt解密代码如下：
        Claims claims = JwtUtil.parseJWT(token);
        Integer userId = Integer.valueOf(claims.get("id").toString());
        System.out.println("用户ID：" + userId);
        return JsonResult.success(token);
    }
}
